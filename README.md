# Job Portal

app simple job-portal with love Spring Boot.

## Required tools should be installed before You run the application
1. [JDK11](https://adoptopenjdk.net/)
2. [Maven](https://maven.apache.org/)
3. [Mysql](https://www.mysql.com/)

## Things to do list:
1. Run Mysql
2. run src/main/resources/user.sql
3. Clone this project: `git clone https://gitlab.com/Akhinahasan/job-portal.git`
4. Go inside the folder: `cd job-portal`
5. Run the application: `mvn clean spring-boot:run`

## To see the available REST API endpoint:
Open your browser then go to [Swagger UI](http://localhost:8080/swagger-ui/index.html)
