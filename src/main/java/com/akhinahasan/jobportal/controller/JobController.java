package com.akhinahasan.jobportal.controller;

import com.akhinahasan.jobportal.dto.JobDto;
import com.akhinahasan.jobportal.service.JobService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/recruitment")
public class JobController {
    private final JobService jobService;

    public JobController(JobService jobService) {
        this.jobService = jobService;
    }

    @GetMapping(value = "/positions.json")
    @ResponseBody
    @ApiOperation(
            value = "Get list job data",
            notes = "return  list  job and get data from url ")
    public ResponseEntity<List<JobDto>> getJobList(){
        return ResponseEntity.ok(jobService.getListJob());
    }


    @GetMapping(value = "/positions/{ID}")
    @ResponseBody
    @ApiOperation(
            value = "Get detail job data",
            notes = "return  detail  job data ")
    public ResponseEntity<JobDto> getJobById( @PathVariable("ID") String jobId){
        return ResponseEntity.ok(jobService.getDetailById(jobId));
    }
}
