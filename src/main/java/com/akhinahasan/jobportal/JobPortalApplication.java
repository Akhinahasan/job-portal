package com.akhinahasan.jobportal;

import com.akhinahasan.jobportal.entity.User;
import com.akhinahasan.jobportal.repository.UserRepository;
import com.akhinahasan.jobportal.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.Optional;

@SpringBootApplication
public class JobPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobPortalApplication.class, args);
	}


	@Bean
	CommandLineRunner init(UserService userService) {


		return args -> {
			userService.createUserAdmin();
		};

	}

}
