package com.akhinahasan.jobportal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDto {
    private String id;

    private String type;

    private String url;

    private String company;

    private String companyUrl;

    private String createdAt;

    private String location;

    private String title;

    private String description;

    private String howToApply;

    private String companyLogo;
}
