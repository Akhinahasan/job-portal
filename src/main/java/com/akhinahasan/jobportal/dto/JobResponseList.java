package com.akhinahasan.jobportal.dto;

import com.akhinahasan.jobportal.entity.Job;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class JobResponseList {
    private List<Job> jobList;
    public JobResponseList(){
        jobList = new ArrayList<>();
    }
}
