package com.akhinahasan.jobportal.service;

import com.akhinahasan.jobportal.entity.User;
import com.akhinahasan.jobportal.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, PasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new RuntimeException("Invalid Username or Password"));
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), user.getAuthorities());

    }

    public void createUserAdmin(){
        Optional<User> optionalUser = userRepository.findByUserName("admin");
        if(optionalUser.isEmpty()){
            User user = User.builder()
                    .userName("admin")
                    .createdAt(LocalDateTime.now())
                    .email("admin@contact.com")
                    .password(bCryptPasswordEncoder.encode("password"))
                    .build();
            userRepository.save(user);
        }
    }
}
