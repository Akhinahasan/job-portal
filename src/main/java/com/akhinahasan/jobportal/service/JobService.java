package com.akhinahasan.jobportal.service;

import com.akhinahasan.jobportal.dto.JobDto;
import com.akhinahasan.jobportal.dto.JobResponseList;
import com.akhinahasan.jobportal.entity.Job;
import com.akhinahasan.jobportal.repository.JobRepository;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class JobService {
    public static final String URL_GET_LIST_JOB_POSITIONS = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
    private final JobRepository jobRepository;
    private final ModelMapper modelMapper;
    private final RestTemplate restTemplate;


    public JobService(JobRepository jobRepository, ModelMapper modelMapper, RestTemplate restTemplate) {
        this.jobRepository = jobRepository;
        this.modelMapper = modelMapper;
        this.restTemplate = restTemplate;
    }

    @SneakyThrows
    public JobDto getDetailById(String jobId){
        Job job = jobRepository.findById(jobId)
                .orElseThrow(() -> new RuntimeException("Job not Found"));
        return modelMapper.map(job,JobDto.class);
    }

    @Transactional
    public List<JobDto> getListJob(){
        Job[] response = restTemplate.getForObject(URL_GET_LIST_JOB_POSITIONS, Job[].class);
        if(Objects.nonNull(response)){
            List<Job> jobList = Arrays.stream(response).collect(Collectors.toList());
            jobRepository.saveAll(jobList);
        }

     return jobRepository.findAll().stream().map(job -> modelMapper.map(job,JobDto.class)).collect(Collectors.toList());
    }
}
