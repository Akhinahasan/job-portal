CREATE TABLE t_user
(
    user_id             int  NOT NULL
        PRIMARY KEY AUTO_INCREMENT,
    user_name             varchar(50)  NOT NULL UNIQUE,
    email                varchar(50)  NOT NULL UNIQUE,
    password             varchar(191) NOT NULL,
    created_at           timestamp NULL,
    updated_at           timestamp NULL
) COLLATE=utf8mb4_unicode_ci;

CREATE TABLE t_job
(
    id             varchar(50)  NOT NULL PRIMARY KEY,
    type             varchar(50),
    url             varchar(150),
    company                varchar(200),
    description             text,
    location             varchar(200),
    company_url           varchar(100) NULL,
    title             varchar(150),
    created_at           VARCHAR(100) NULL,
    how_to_apply text,
    company_logo text
) COLLATE=utf8mb4_unicode_ci;
